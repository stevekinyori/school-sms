-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2016 at 03:08 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `academicyears`
--

CREATE TABLE IF NOT EXISTS `academicyears` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  `createdBy` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `classId` int(10) NOT NULL AUTO_INCREMENT,
  `streamId` int(10) NOT NULL,
  `classCapacity` int(10) NOT NULL,
  `classNo` int(10) NOT NULL,
  `datecreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(4) NOT NULL,
  `updatedBy` int(10) DEFAULT NULL,
  `dateUpdated` date DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`classId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classsubject`
--

CREATE TABLE IF NOT EXISTS `classsubject` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `classId` int(10) NOT NULL,
  `subjectId` int(10) NOT NULL,
  `teacherId` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `updatedBy` int(10) DEFAULT NULL,
  `dateUpdated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `examresults`
--

CREATE TABLE IF NOT EXISTS `examresults` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `examId` int(10) NOT NULL,
  `classId` int(10) NOT NULL,
  `studentId` int(10) NOT NULL,
  `subjectId` int(10) NOT NULL,
  `mark` int(15) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE IF NOT EXISTS `exams` (
  `examId` int(10) NOT NULL AUTO_INCREMENT,
  `termId` int(10) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`examId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `examsettings`
--

CREATE TABLE IF NOT EXISTS `examsettings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `examId` int(10) NOT NULL,
  `classId` int(10) NOT NULL,
  `termId` int(10) NOT NULL,
  `subjectId` int(10) NOT NULL,
  `examDate` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `maxMark` int(10) NOT NULL DEFAULT '100',
  `passMark` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feeallocation`
--

CREATE TABLE IF NOT EXISTS `feeallocation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `feeCategory` int(10) NOT NULL,
  `feeSubCategory` int(10) NOT NULL,
  `Applicationlevel` int(10) NOT NULL,
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feeapplicationlevels`
--

CREATE TABLE IF NOT EXISTS `feeapplicationlevels` (
  `levelId` int(10) NOT NULL AUTO_INCREMENT,
  `Name` int(30) NOT NULL,
  `description` int(50) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`levelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedates`
--

CREATE TABLE IF NOT EXISTS `feedates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subCategoryId` int(10) NOT NULL,
  `startDate` date NOT NULL,
  `dueDate` date NOT NULL,
  `endDate` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feescategory`
--

CREATE TABLE IF NOT EXISTS `feescategory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `receiptPrefix` varchar(50) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `dateCreated` date NOT NULL,
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  `createdBy` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feessubcategory`
--

CREATE TABLE IF NOT EXISTS `feessubcategory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `categoryId` int(10) NOT NULL,
  `subcategoryName` varchar(20) NOT NULL,
  `Amount` int(100) NOT NULL,
  `feeType` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feewaivers`
--

CREATE TABLE IF NOT EXISTS `feewaivers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `feeCategoryId` int(10) NOT NULL,
  `feeSubCategoryId` int(10) NOT NULL,
  `waiverType` varchar(30) NOT NULL,
  `dateCreated` date NOT NULL,
  `categoryId` int(10) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `rate` int(10) NOT NULL,
  `rateType` varchar(10) NOT NULL DEFAULT 'percent',
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  `createdBy` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feeSubCategoryId` (`feeSubCategoryId`),
  KEY `feeCategoryId_2` (`feeCategoryId`),
  KEY `categoryId` (`categoryId`),
  KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gradescale`
--

CREATE TABLE IF NOT EXISTS `gradescale` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `examId` int(10) NOT NULL,
  `subjectId` int(10) NOT NULL,
  `classId` int(10) NOT NULL,
  `grade` varchar(1) NOT NULL,
  `lowerLimit` int(10) NOT NULL,
  `upperLimit` int(10) NOT NULL,
  `limitType` varchar(10) NOT NULL DEFAULT 'percent',
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`),
  KEY `subjectId` (`subjectId`),
  KEY `classId` (`classId`),
  KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE IF NOT EXISTS `guardian` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `middleName` varchar(50) NOT NULL,
  `initials` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) NOT NULL,
  `occupation` varchar(50) NOT NULL,
  `phoneNo` varchar(50) DEFAULT NULL,
  `studentId` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `dateUpdated` date DEFAULT NULL,
  `updatedBy` int(10) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `studentId` (`studentId`),
  KEY `createdBy` (`createdBy`),
  KEY `updatedBy` (`updatedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lib_books`
--

CREATE TABLE IF NOT EXISTS `lib_books` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `isbn_No.` int(10) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `edition` int(10) DEFAULT NULL,
  `book_no` int(10) DEFAULT NULL,
  `author` varchar(50) NOT NULL,
  `status` varchar(10) DEFAULT 'Active',
  `publisher` varchar(50) DEFAULT NULL,
  `categoryId` int(10) NOT NULL,
  `No_of_Copies` int(10) DEFAULT NULL,
  `Shelf_No` int(10) NOT NULL,
  `position` int(10) NOT NULL,
  `cost` int(10) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `condition` varchar(20) DEFAULT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`),
  KEY `createdBy` (`createdBy`),
  KEY `categoryId_2` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lib_category`
--

CREATE TABLE IF NOT EXISTS `lib_category` (
  `id` int(10) NOT NULL,
  `name` int(10) NOT NULL,
  `code` int(10) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `dateCreated` date NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `createdBy` (`createdBy`),
  KEY `createdBy_2` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lib_issued_books`
--

CREATE TABLE IF NOT EXISTS `lib_issued_books` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userType` varchar(20) NOT NULL,
  `userId` int(10) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `dueDate` date NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Issued',
  `issue_condition` varchar(30) NOT NULL,
  `return_condition` varchar(30) DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  `issue_comments` varchar(100) DEFAULT NULL,
  `issuedBy` int(10) NOT NULL,
  `receivedBy` int(10) DEFAULT NULL,
  `return_comments` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
   KEY `userId` (`userId`),
  KEY `issuedBy` (`issuedBy`),
  KEY `receivedBy` (`receivedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE IF NOT EXISTS `parent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `middleName` varchar(50) NOT NULL,
  `initials` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) NOT NULL,
  `occupation` varchar(50) NOT NULL,
  `phoneNo` varchar(50) DEFAULT NULL,
  `studentId` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `dateUpdated` date DEFAULT NULL,
  `updatedBy` int(10) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `createdBy` (`createdBy`),
  KEY `studentId` (`studentId`),
  KEY `updatedBy` (`updatedBy`,`studentId`,`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `previlages`
--

CREATE TABLE IF NOT EXISTS `previlages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  PRIMARY KEY (`id`),
   KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roleprevilage`
--

CREATE TABLE IF NOT EXISTS `roleprevilage` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `roleId` int(10) NOT NULL,
  `previlageId` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  PRIMARY KEY (`id`),
   KEY `roleId` (`roleId`),
   KEY `previlageId` (`previlageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `Name` varchar(50) NOT NULL,
  `Initials` varchar(20) NOT NULL,
  `location` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Capacity` int(50) NOT NULL,
  `Logo` varchar(50) NOT NULL,
  `Motto` varchar(100) NOT NULL,
  `Vision` varchar(500) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `streams`
--

CREATE TABLE IF NOT EXISTS `streams` (
  `streamId` int(10) NOT NULL AUTO_INCREMENT,
  `streamName` varchar(50) NOT NULL,
  `streamInitials` varchar(10) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  PRIMARY KEY (`streamId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `studentId` int(10) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `initials` varchar(10) DEFAULT NULL,
  `DOB` date NOT NULL,
  `dateCreated` date NOT NULL,
  `WEF` date NOT NULL,
  `WET` date DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `bloodGroup` varchar(50) DEFAULT NULL,
  `Religion` varchar(50) DEFAULT NULL,
  `Present Address` varchar(50) DEFAULT NULL,
  `Permanent Address` varchar(50) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Mobile` varchar(50) DEFAULT NULL,
  `prevSchool` varchar(50) DEFAULT NULL,
  `schoolAddress` varchar(50) DEFAULT NULL,
  `qualifications` varchar(50) DEFAULT NULL,
  `entryClass` int(10) NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(4) NOT NULL DEFAULT '0',
  `updatedBy` int(10) DEFAULT NULL,
  `dateUpdated` date DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`studentId`),
   KEY `createdBy` (`createdBy`),
   KEY `entryClass` (`entryClass`),
   KEY `updatedBy` (`updatedBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `subjectId` int(10) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(20) NOT NULL,
  `subjectCode` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `dateUpdated` date NOT NULL,
  `updatedBy` int(10) NOT NULL,
  PRIMARY KEY (`subjectId`),
   KEY `updatedBy` (`updatedBy`),
   KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
  `teacherId` int(10) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(20) NOT NULL,
  `middleName` varchar(20) NOT NULL,
  `initials` varchar(10) DEFAULT NULL,
  `surname` varchar(20) NOT NULL,
  `otherNames` varchar(20) DEFAULT NULL,
  `dateOfBirth` date NOT NULL,
  `pinNo` int(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `mobileNo` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `DateOfHiring` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `dateUpdated` date DEFAULT NULL,
  `updatedBy` int(10) DEFAULT NULL,
  PRIMARY KEY (`teacherId`),
   KEY `updatedBy` (`updatedBy`),
  KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE IF NOT EXISTS `terms` (
  `termId` int(10) NOT NULL AUTO_INCREMENT,
  `academicYearId` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  `WEF` date NOT NULL,
  `WET` date NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`termId`),
   KEY `academicYearId` (`academicYearId`),
   KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `secretQuestion` varchar(25) DEFAULT NULL,
  `secretAnswer` varchar(25) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `roleId` int(10) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  PRIMARY KEY (`id`),
   KEY `roleId` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `desc` varchar(500) NOT NULL,
  `dateCreated` date NOT NULL,
  `createdBy` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gradescale`
--
ALTER TABLE `gradescale`
  ADD CONSTRAINT `gradescale_ibfk_3` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `gradescale_ibfk_1` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`subjectId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `gradescale_ibfk_2` FOREIGN KEY (`classId`) REFERENCES `classes` (`classId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guardian`
--
ALTER TABLE `guardian`
  ADD CONSTRAINT `guardian_ibfk_3` FOREIGN KEY (`updatedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `guardian_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `guardian_ibfk_2` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lib_books`
--
ALTER TABLE `lib_books`
  ADD CONSTRAINT `lib_books_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `lib_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lib_books_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lib_category`
--
ALTER TABLE `lib_category`
  ADD CONSTRAINT `lib_category_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lib_issued_books`
--
ALTER TABLE `lib_issued_books`
  ADD CONSTRAINT `lib_issued_books_ibfk_3` FOREIGN KEY (`receivedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lib_issued_books_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lib_issued_books_ibfk_2` FOREIGN KEY (`issuedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `parent`
--
ALTER TABLE `parent`
  ADD CONSTRAINT `parent_ibfk_3` FOREIGN KEY (`studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `parent_ibfk_1` FOREIGN KEY (`updatedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `parent_ibfk_2` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `previlages`
--
ALTER TABLE `previlages`
  ADD CONSTRAINT `previlages_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `roleprevilage`
--
ALTER TABLE `roleprevilage`
  ADD CONSTRAINT `roleprevilage_ibfk_2` FOREIGN KEY (`previlageId`) REFERENCES `previlages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `roleprevilage_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `user_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_3` FOREIGN KEY (`entryClass`) REFERENCES `classes` (`classId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_ibfk_2` FOREIGN KEY (`updatedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `subject_ibfk_2` FOREIGN KEY (`updatedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `subject_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_ibfk_2` FOREIGN KEY (`updatedBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `terms`
--
ALTER TABLE `terms`
  ADD CONSTRAINT `terms_ibfk_2` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `terms_ibfk_1` FOREIGN KEY (`academicYearId`) REFERENCES `academicyears` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `user_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
